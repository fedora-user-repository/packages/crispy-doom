# yapt spec
### Build yapt from the git repository.

This repository defines how yapt should be built from source from the live git repository. 
It does not use versioned releases, and simply builds the latest commit. It can be used 
as a template for other packages made for and submitted to the Fedora User Repository (FUR). 
A package MUST include a spec file, to define how the RPM package should be built. For 
dependencies that have package definitions in the FUR, you can simply add them as you 
would any other dependency in the spec file. The FUR helper, `yapt`, will attempt to find 
these dependencies in the FUR and install them before building your packages with `fedpkg`. 
You may include a `README.md` file and a `LICENSE` file if necessary. Try to keep the 
repository that defines how to build your package as minimal as possible. Do not include 
and binaries or compressed archives. These should merely be scripts and spec files that 
define how a package should be built. Including packages, compressed archives, or other 
pieces of software may be violating a license agreement. Only include files that you have 
created yourself from scratch and have the legal rights to share.
