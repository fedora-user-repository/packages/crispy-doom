# Maintainer Nick Girga <nickgirga@gmail.com>
Name:     crispy-doom
Version:  6.0
Release:  1
Summary:  A limit-removing Doom source port
License:  GPL-2.0

# Project URL
URL:  https://github.com/fabiangreffrath/crispy-doom

# URL to compressed archive
Source0:  https://github.com/fabiangreffrath/crispy-doom/archive/refs/tags/%{name}-%{version}.tar.gz

# Build Dependencies
BuildRequires:  bash
BuildRequires:  coreutils
BuildRequires:  make
BuildRequires:  python3
BuildRequires:  SDL2
BuildRequires:  SDL2-devel
BuildRequires:  SDL2_mixer
BuildRequires:  SDL2_mixer-devel
BuildRequires:  SDL2_net
BuildRequires:  SDL2_net-devel

# Runtime Dependencies
Requires:  fluidsynth
Requires:  hicolor-icon-theme
Requires:  libpng
Requires:  libsamplerate
Requires:  SDL2_mixer
Requires:  SDL2_net

%description
Crispy Doom is a friendly fork of Chocolate Doom that 
provides a higher display resolution, removes the 
static limits of the Doom engine and offers further 
optional visual, tactical and physical enhancements 
while remaining entirely config file, savegame, 
netplay and demo compatible with the original.

%prep
%autosetup -n %{name}-%{name}-%{version}

# TODO!: apply patches
# from crispy-doom PKGBUILD (on AUR)

%build
# from crispy-doom PKGBUILD (on AUR)
./autogen.sh --prefix=/usr
make

%install
# from crispy-doom PKGBUILD (on AUR)
make DESTDIR="%{buildroot}" install

cd "%{buildroot}/usr"
rm -rf share/man/man5/default.cfg.5              \
       share/man/man5/heretic.cfg.5              \
       share/man/man5/hexen.cfg.5                \
       share/man/man5/strife.cfg.5               \
       share/man/man6/chocolate-{server,setup}.6

rm bin/crispy-{heretic,hexen,strife}-setup
mv bin/crispy-doom-setup bin/crispy-setup
for game in doom heretic hexen strife; do
  ln -s crispy-setup bin/crispy-${game}-setup
done

exa -lT

%files
%{_bindir}/**
%{_datadir}/**

%changelog
* Wed Feb 7 2024 Nick Girga <nickgirga@gmail.com>
- Make MIDI device selection less ambiguous
- Add default difficulty option (by kiwaph and @mikeday0)
- Improve smoothness of network games when running uncapped (thanks @rfomin)
- Disable smooth pixel scaling if software rendering is enforced
- Add framerate limiting option (@mikeday0 and @rfomin)
- All music formats now work when the OPL backend is selected (@rfomin)
- Implement demo footer for Doom and Heretic (@rfomin)
- several bug fixes
- several game-specific improvements
- see full changelog at https://github.com/fabiangreffrath/crispy-doom/releases/tag/crispy-doom-6.0
